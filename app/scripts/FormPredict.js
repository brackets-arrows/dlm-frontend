import React, { Component } from 'react';
import {select, edit, add, markAllQueryUnselected} from './db_funcs';
import _, {first} from 'underscore';
import Formsy  from 'formsy-react';
import {MySelect} from './FormComponents';
import SortableListItem from './dumb/SortableList.js';

let FormPredict = React.createClass({
    editState (data) {
        let {id, state} = first(select({type: "predict",selected: true}));
        edit({type: "predict", id: id, state: parseInt(data.state)});
    },
    reorderInputs (oldIdx, newIdx) {
        //console.log(oldIdx, newIdx);
        let {id, inputs} = first(select({type: "predict",selected: true}));
        let inputsId = inputs.map(i => i.id);
        //console.log(JSON.stringify(inputsId));
        const [val] = inputsId.splice(oldIdx, 1);
        inputsId.splice(newIdx, 0, val);
        //console.log(JSON.stringify(inputsId));

        edit({type: "predict", id: id,"inputs": inputsId});
    },
    addInput () {
        let addId = parseInt(React.findDOMNode(this.refs.inputsAdded).value);
        let {id, inputs} = select({type: "predict",selected: true})[0] || {};

        if (id) {
            let newInputs =  inputs.map(e => e.id).concat([addId]);
            edit({type: "predict", id: id,"inputs": newInputs});
        } else {
            let state = parseInt(React.findDOMNode(this.refs.state).value);
            let id = add({type: "predict", "inputs":[addId], state});
            edit({type:"predict",id, selected: true});
        }
    },
    reset () {
        markAllQueryUnselected();
    },
    removeHandle (idx) {
        let {id, inputs} = first(select({type: "predict",selected: true}));
        let inputsId = inputs.map(i => i.id);
        inputsId.splice(idx, 1);
        edit({type: "predict", id: id,"inputs": inputsId});
    },
    render () {
        let {id: selectedId = null, state: selectedState = {}, inputs: selectedInputs = []} = select({type: "predict",selected: true})[0] || {};
        let count = 0;
        return (
                <div className="ui bottom attached two column grid segment" id="query_form">
                    <div className="column">
                        <div className="ui attached tab active segment">
                            <Formsy.Form ref="state_form" onValidSubmit={this.editState} onChange={selectedId ? () => this.refs.state_form.submit() : () => {}}>
                                <div className="ui form">
                                    <div className="ui action input">
                                        <MySelect name="state" ref="state" initValue={selectedId} options={select({type: "state", onCanvas: true}).map(({id, name}) => ({value:id, text: name}))}/>
                                    </div>
                                </div>
                            </Formsy.Form>
                            {selectedId ?
                                <div className="ui celled large selection list">
                                    <button onClick={this.reset} className="circular ui icon button">
                                        <i className="icon history"></i>
                                    </button>
                                </div>
                                : ""
                            }
                        </div>
                    </div>
                    <div className="column">
                        <div className="ui attached tab active segment">
                            <div className="ui form">
                                <div className="ui action input">
                                    <select name="input" ref="inputsAdded">
                                        {select({type: "input", onCanvas: true}).map(({id, name}) => (<option key={id} value={id}>{name}</option>))}
                                    </select>
                                    <button onClick={this.addInput} className="ui teal icon button">
                                        <i className="plus icon"></i>
                                    </button>
                                </div>
                            </div>
                            {selectedInputs.length
                                ? <div className="ui celled large selection list">
                                    {selectedInputs.map(({id, name}, i) => (<SortableListItem key={i} index={i} id={id} text={name} type="sortableInput" move={this.reorderInputs} remove={this.removeHandle}/>))}
                                </div>
                                : ""
                            }
                        </div>
                    </div>
                </div>
        );
    }
});
export default FormPredict;
