import _, {zip, min} from 'underscore';

export function topOfCircle(stateData, r) {
  var r = r || 50;
  return {
    "x": stateData.x,
    "y": stateData.y - r
  };
};

export function botOfCircle (stateData, r) {
  var r = r || 50;
  return {
    "x": stateData.x,
    "y": stateData.y + r
  };
};

export function lenOfLine(linePositions) {
  return Math.sqrt(
    Math.pow((linePositions.endX - linePositions.startX), 2) +
    Math.pow((linePositions.endY - linePositions.startY), 2));
};

export function arrBegin(linePositions, arrLen) {
  var lineLen = lenOfLine(linePositions),
      rel = (lineLen - arrLen) / arrLen,
      lineAx = linePositions.startX,
      lineAy = linePositions.startY,
      lineBx = linePositions.endX,
      lineBy = linePositions.endY,
      coordX, coordY;

  coordX = (lineAx + rel*lineBx) / (1 + rel);
  coordY = (lineAy + rel*lineBy) / (1 + rel);
  return {x: coordX, y: coordY};
};

export function test(linePositions, arrLen) {
  var arrLen = arrLen || 8, // 8px by default
      end = {x: linePositions.endX, y: linePositions.endY},
      begin = arrBegin(linePositions, arrLen),
      x1, x2, y1, y2, midOfLine;
  // x3
  var top = 5*5 * Math.pow((end.y - begin.y), 2);
  var bot = Math.pow((end.y - begin.y), 2) + Math.pow((end.x - begin.x), 2)
  var x3 = Math.sqrt(top/bot) + begin.x;

  // y3
  var y3 = Math.sqrt(5*5 - Math.pow((x3-begin.x), 2)) + begin.y

  var diffX = Math.abs(begin.x - x3);
  var diffY = Math.abs(begin.y - y3);

  if ((begin.x <= end.x && begin.y <= end.y) || (begin.x >= end.x && begin.y >= end.y)) {
    x1 = begin.x - diffX;
    y1 = begin.y + diffY;

    x2 = begin.x + diffX;
    y2 = begin.y - diffY;
  }

  if ((begin.x <= end.x && begin.y >= end.y) || (begin.x >= end.x && begin.y <= end.y)) {
    x1 = begin.x - diffX;
    y1 = begin.y - diffY;

    x2 = begin.x + diffX;
    y2 = begin.y + diffY;
  }

  //console.log(x1, y1, x2, y2);

  return [{x: x1, y: y1}, {x: x2, y: y2}];
}


export function getMidOfLine(begin, end) {
  return {
    x: (begin.x + end.x) / 2,
    y: (begin.y + end.y) / 2
  };
}

function solveQuadraticEquation(a, b, c) {
  var d = b * b - 4 * a * c,
      ds,
      mbmds,
      mbpds;
  if (a === 0) {
    // linear equation
    if (b === 0) {
      if (c === 0) {
        // all values of x are ok.
        return [undefined, undefined, undefined];
      } else {
        return [];
      }
    } else {
      return [-c / b];
    }
  }

  if (d < 0) {
    return [];
  } else if (d === 0) {
    return [-b / (2 * a)];
  }

  ds = Math.sqrt(d);
  if (b >= 0) {
    mbmds = -b - ds;
    return [mbmds / (2 * a), 2 * c / mbmds];
  } else {
    mbpds = -b + ds;
    return [2 * c / mbpds, mbpds / (2 * a)];
  }
}

export function getIntersect (p1, p2, r) {
	if (Math.abs(p1.x - p2.x) < Number.EPSILON) {
		return [{x:p1.x,y:p1.y + r},{x:p1.x,y:p1.y - r}]
	};
	if (Math.abs(p1.y - p2.y) < Number.EPSILON) {
		return [{y:p1.y,x:p1.x + r},{y:p1.y,x:p1.x - r}]
	};
	var kL = ((p2.y - p1.y) / (p2.x - p1.x));
	var bL = ((p2.x * p1.y  - p1.x * p2.y) / (p2.x - p1.x));
	//console.log(kL, bL);
	var AC = -2 * p1.x;
	var BC = -2 * p1.y;
	var CC = Math.pow(AC, 2) / 4 + Math.pow(BC, 2) / 4 - Math.pow(r, 2);
	var aE = 1 + Math.pow(kL, 2);
	var bE = 2 *kL *bL + AC + BC * kL;
	var cE = Math.pow(bL, 2) + BC * bL + CC;
	//console.log(aE, bE, cE);
	return solveQuadraticEquation(aE, bE, cE).map(function (x) {
		var y = kL *x + bL;
		return {x: x, y: y};
	});
}


export function getNearestPoints (p1, p2, r) {
    let points1 = getIntersect(p1, p2, r);
    let points2 = getIntersect(p2, p1, r);
    let allPairs = zip(points1, points2).concat(zip(points1, points2.reverse()));
    //console.log(JSON.stringify(allPairs));
    //console.log(JSON.stringify(allPairs.map(([{x:startX,y:startY}, {x: endX, y: endY}]) => Math.abs(lenOfLine({startX, startY, endX, endY})))));
    return min(allPairs, ([{x:startX,y:startY}, {x: endX, y: endY}]) => Math.abs(lenOfLine({startX, startY, endX, endY})));
}


export function minLenOfLine(pairs) {
    return min(pairs, ([{x:startX,y:startY}, {x: endX, y: endY}]) => Math.abs(lenOfLine({startX, startY, endX, endY})));
}

function getCanonCoeff (begin, end) {
	return [begin.y - end.y, end.x - begin.x, begin.x * end.y - end.x * begin.y];
}
export function getSlopeCoeff (begin, end) {
    let [a,b,c] = getCanonCoeff(begin, end);
	return Math.abs(b) > Number.MIN_VALUE ? -(a / b) : 0;
}

export function getPerpendicularOffset (begin, end, length) {
    let [a,b,c] = getCanonCoeff(begin, end);
	if (Math.abs(a) < Number.MIN_VALUE) {
		return {x: length, y: 0};
	};
    let diff = {};
    diff.x = (length / Math.sqrt(Math.pow(b/a, 2) + 1));
    diff.y = (b/a * diff.x);
	return diff;
}
